(function() {
    "use strict";
    define(["jquery", "canvas", "raphael"], function($, canvas, Raphael) {
        function multiplyMatrices(a, b) {
            var result = [];
            for(var i = 0; i < 3; i++) {
                var positionalResult = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
                for(var j = 0; j < 3; j++) {
                    for(var k = 0; k < 3; k++) {
                        positionalResult[i][j] += a[i][k] * b[k][j];
                    }
                }
                result.push(positionalResult[i]);
            }
            return result;
        }

        Raphael.st.draggable = function() {
            var elementBBox = polygon.element.getBBox();
            var rectAttrs = {
                x: elementBBox.x,
                y: elementBBox.y,
                width: elementBBox.x2 - elementBBox.x,
                height: elementBBox.y2 - elementBBox.y
            };
            var eventRectangle = canvas.rect(rectAttrs.x, rectAttrs.y, rectAttrs.width, rectAttrs.height);
            eventRectangle.attr("fill", "rgba(0, 0, 0, 0)").attr("stroke", "white");
            eventRectangle.drag(polygon.dragMove, polygon.dragStart, polygon.dragEnd);
        };

        var polygon = {
            matrix: [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
            centroid: {
                x: 0,
                y: 0
            },
            element: canvas.set(),
            points: [],
            addPointCoordinates: function(event) {
                var coordinates = {
                    x: event.pageX,
                    y: event.pageY,
                    ox: event.pageX,
                    oy: event.pageY
                };
                canvas.circle(coordinates.x, coordinates.y, 2).attr("fill", "#000");
                polygon.points.push(coordinates);
            },
            addPathBetween: function(start, end) {
                var path = canvas.path("M" + start.x + "," + start.y + "L" + end.x + "," + end.y).attr({
                    fill: "#fff",
                    stroke: "#000"
                });
                polygon.element.push(path);
            },
            draw: function(event) {
                if(!!event) {
                    event.preventDefault();
                    if(polygon.points.length < 3) {
                        alert("Add 3 or more points, this is supposed to be a polygon.");
                        return;
                    }
                    $(document).off(".draw");
                }

                canvas.clear();
                for(var i = 0; i < polygon.points.length - 1; i++) {
                    polygon.addPathBetween(polygon.points[i], polygon.points[i+1]);
                }
                polygon.addPathBetween(polygon.points[0], polygon.points[i]);
                polygon.centroid = polygon.getCentroid();
                polygon.element.draggable();
                canvas.circle(polygon.centroid.x, polygon.centroid.y, 3);
            },
            getCentroid: function() {
                var centroid = {
                    x: 0,
                    y: 0
                };
                for(var i = 0, length = polygon.points.length; i < length; i++) {
                    centroid.x += polygon.points[i].x;
                    centroid.y += polygon.points[i].y;
                }
                centroid.x /= length;
                centroid.y /= length;

                return centroid;
            },
            dragStart: function(object) {

            },
            dragMove: function(dx, dy, mx, my, ev) {
                if(ev.shiftKey)
                    polygon.scale(dx);
                else if(ev.ctrlKey)
                    polygon.rotate(dx);
                else
                    polygon.translate(dx, dy);
                polygon.draw();
            },
            dragEnd: function() {
                for(var i = 0; i < polygon.points.length; i++) {
                    polygon.points[i].ox = polygon.points[i].x;
                    polygon.points[i].oy = polygon.points[i].y;
                }
            },
            translate: function(dx, dy) {
                polygon.matrix[0][2] = dx;
                polygon.matrix[1][2] = dy;
                for(var index = 0; index < polygon.points.length; index++) {
                    var resultArray = polygon.applyTransformation(index);
                    polygon.points[index].x = resultArray[0];
                    polygon.points[index].y = resultArray[1];
                }
                polygon.resetMatrix();
            },
            scale: function(dx) {
                var scaleFactor;
                if(!dx) {
                    scaleFactor = 1;
                }else {
                    scaleFactor = dx/1000+1;
                }
                var x = polygon.centroid.x;
                var y = polygon.centroid.y;
                var translateToOriginMatrix = [[1, 0, -x], [0, 1, -y], [0, 0, 1]];
                var scaleMatrix = [[scaleFactor, 0, 0], [0, scaleFactor, 0], [0, 0, 1]];
                var translateBackMatrix = [[1, 0, x], [0, 1, y], [0, 0, 1]];

                polygon.matrix = multiplyMatrices(multiplyMatrices(translateBackMatrix, scaleMatrix), translateToOriginMatrix);
                for(var index = 0; index < polygon.points.length; index++) {
                    var resultArray = polygon.applyTransformation(index);
                    polygon.points[index].x = resultArray[0];
                    polygon.points[index].y = resultArray[1];
                }
                polygon.resetMatrix();
            },
            rotate: function(value) {
                var angle;
                if(!value) {
                    angle = 0;
                }else {
                    angle = value/360;
                }

                var x = polygon.centroid.x;
                var y = polygon.centroid.y;
                var angleCos = Math.cos(angle);
                var angleSin = Math.sin(angle);
                var translateToOriginMatrix = [[1, 0, -x], [0, 1, -y], [0, 0, 1]];
                var rotateMatrix = [[angleCos, -angleSin, 0], [angleSin, angleCos, 0], [0, 0, 1]];
                var translateBackMatrix = [[1, 0, x], [0, 1, y], [0, 0, 1]];

                polygon.matrix = multiplyMatrices(multiplyMatrices(translateBackMatrix, rotateMatrix), translateToOriginMatrix);
                for(var index = 0; index < polygon.points.length; index++) {
                    var resultArray = polygon.applyTransformation(index);
                    polygon.points[index].x = resultArray[0];
                    polygon.points[index].y = resultArray[1];
                }
                polygon.resetMatrix();
            },
            applyTransformation: function(index) {
                var pointArray = [polygon.points[index].ox, polygon.points[index].oy, 1];
                var resultArray = [];
                for(var i = 0; i < 3; i++) {
                    var positionalResult = 0;
                    for(var j = 0; j < 3; j++) {
                        positionalResult += polygon.matrix[i][j] * pointArray[j];
                    }
                    resultArray.push(positionalResult);
                }
                return resultArray;
            },
            resetMatrix: function() {
                polygon.matrix = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
            }
        };
        return polygon;
    });
}());

