require.config({
    paths: {
        jquery: "../vendor/jquery-2.1.4",
        raphael: "../vendor/raphael",
        polygon: "../core/polygon",
        canvas: "../core/canvas"
    },
    shim: {
        jquery: {
            exports: "jQuery"
        },
        raphael: {
            exports: "Raphael"
        },
        canvas: {
            deps: ["jquery", "raphael"]
        },
        polygon: {
            deps: ["jquery", "raphael", "canvas"]
        }
    }
});

require(["canvas", "polygon"], function(canvas, polygon) {
    $(document).on("click.draw", polygon.addPointCoordinates)
        .on("contextmenu.draw", polygon.draw)
});