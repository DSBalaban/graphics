require.config({
    paths: {
        jquery: "../vendor/jquery-2.1.4",
        raphael: "../vendor/raphael",
        canvas: "../core/canvas",
        curve: "../core/curve"
    },
    shim: {
        jquery: {
            exports: "jQuery"
        },
        raphael: {
            exports: "Raphael"
        },
        canvas: {
            deps: ["jquery", "raphael"]
        },
        curve: {
            deps: ["canvas"]
        }
    }
});

require(["canvas", "curve"], function(canvas, curve) {
    curve.generateCurve();
    curve.draw();
    curve.generateAxes();
});