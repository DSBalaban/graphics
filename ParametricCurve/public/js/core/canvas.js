(function() {
    "use strict";
    define(["jquery", "raphael"], function($, Raphael) {
        var canvasSize = {
            width: window.innerWidth,
            height: window.innerHeight
        };

        return Raphael(0, 0, canvasSize.width, canvasSize.height);
    });
}());