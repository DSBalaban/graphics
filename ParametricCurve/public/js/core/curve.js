(function() {
    "use strict";
    define(["canvas"], function(canvas) {
        var curve = {
            center: {
                x: window.innerWidth / 2,
                y: window.innerHeight / 2
            },
            radius: 200,
            points: [],
            limit: 4 * Math.PI,
            a: 20,
            f: function (t) {
                return curve.a*t*Math.cos(t);
            },
            g: function (t) {
                return curve.a*t*Math.sin(t);
            },
            circleF: function(t) {
                return curve.radius * Math.cos(t);
            },
            circleG: function(t) {
                return curve.radius * Math.sin(t);
            },
            generateCurve: function() {
                for(var i = 0; i < curve.limit; i+=0.001) {
                    curve.points.push({
                        x: curve.f(i) + curve.center.x,
                        y: curve.g(i) + curve.center.y
                    });
                }
            },
            generateAxes: function() {
                curve.addPathBetween({x: 20, y: curve.center.y}, {x: window.innerWidth - 20, y: curve.center.y},
                    "classic-wide-long");
                curve.addPathBetween({x: curve.center.x, y: window.innerHeight - 20},
                    {x: curve.center.x, y: 20}, "classic-wide-long");
            },
            draw: function() {
                canvas.clear();
                for(var i = 0, len = curve.points.length - 1; i < len; i++) {
                    curve.addPathBetween(curve.points[i], curve.points[i+1]);
                }
            },
            addPathBetween: function(start, end, arrowEnd) {
                var path = canvas.path("M" + start.x + "," + start.y + "L" + end.x + "," + end.y).attr({
                    fill: "#fff",
                    stroke: "#000"
                });
                if(!!arrowEnd)
                    path.attr("arrow-end", arrowEnd);
            }
        };

        return curve;
    });
}());