Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

(function($) {
    var canvasSize = {
        width: window.innerWidth - 20,
        height: window.innerHeight - 20
    };
    var canvas = Raphael(10, 10, canvasSize.width, canvasSize.height);

    $.get("config/data.json")
        .done(barChart)
        .fail(function(error) {
            console.error(error);
        });

    function scaleHeight(data) {
        var scaleHeightFactor = 1;
        for(var key in data) {
            if(data.hasOwnProperty(key) && data[key] > canvasSize.height) {
                var tempFactor = canvasSize.height / data[key];
                scaleHeightFactor = scaleHeightFactor > tempFactor ? tempFactor : scaleHeightFactor;
            }
        }
        return scaleHeightFactor;
    }

    function barChart(data) {
        var scaleHeightFactor = scaleHeight(data);
        var dataSize = Object.size(data);
        var width = (canvasSize.width / dataSize);
        var iterator = 0;
        var rectangleSet = canvas.set();
        $.each(data, function(index, value) {
            var actualValue = value * scaleHeightFactor;
            var rectangle = canvas.rect(iterator*width+5, canvasSize.height - actualValue, width - 5, actualValue);
            rectangle.attr("fill", "teal");
            rectangle.attr("stroke", "#000");
            rectangleSet.push(rectangle);
            iterator++;
        });
    }
}(jQuery));