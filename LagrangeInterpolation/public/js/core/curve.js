(function() {
    "use strict";
    define(["canvas"], function(canvas) {
        var curve = {
            points: [],
            f: function(t) {
                var result = 0;
                for(var i = 0; i < curve.points.length; i++) {
                    var numerator = 1;
                    var denominator = 1;
                    for(var j = 0; j < curve.points.length; j++) {
                        if(i !== j) {
                            numerator *= t - curve.points[j].x;
                            denominator *= curve.points[i].x - curve.points[j].x;
                        }
                    }
                    result += (numerator / denominator) * curve.points[i].y;
                }

                return result;
            },
            draw: function(event) {
                event.preventDefault();
                $(document).off(".draw");
                var endPoint = curve.points[curve.points.length-1];
                for(var i = curve.points[0].x; i < endPoint.x; i+=0.1) {
                    curve.addPointAt(i, curve.f(i));
                }
            },
            addPointOnClick: function(event) {
                var coordinates = {
                    x: event.pageX,
                    y: event.pageY
                };
                canvas.circle(coordinates.x, coordinates.y, 5).attr("fill", "red");
                curve.points.push(coordinates);
            },
            addPointAt: function(x, y) {
                canvas.circle(x, y, 1);
            }
        };

        return curve;
    });
}());